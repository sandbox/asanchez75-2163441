<?php

/** @file
 * It contains main functions
 * Implementation of hook_menu
 * @return [type] [description]
 */
function realname_autocomplete_widget_menu() {
  $items['realname_autocomplete_widget/autocomplete'] = array(
    'title' => 'Autocomplete Realname',
    'page callback' => 'realname_autocomplete_widget_autocomplete',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implements hook_field_widget_info().
 */
function realname_autocomplete_widget_field_widget_info() {
  return array(
    'realname_autocomplete_widget' => array(
      'label' => t('Autocomplete Realname'),
      'field types' => array('text'),
      'settings' => array(
        'size' => 60,
        'autocomplete_path' => 'realname_autocomplete_widget/autocomplete',
        ),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function realname_autocomplete_widget_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  switch ($instance['widget']['type']) {
    case 'realname_autocomplete_widget':
      $element['value'] = $element + array(
        '#type' => 'textfield',
        '#default_value' => isset($items[$delta]['value']) ? $items[$delta]['value'] : NULL,
        '#autocomplete_path' => $instance['widget']['settings']['autocomplete_path'] . '/' . $field['field_name'],
        '#size' => $instance['widget']['settings']['size'],
        '#maxlength' => 1024,
      );
      break;
  }
  return $element;
}

/**
 * Generate an option list to display suggestion terms
 * @param  string $field_name [description]
 * @param  string $name       [description]
 * @return [type]             [description]
 */
function realname_autocomplete_widget_autocomplete($field_name = '', $name = '') {
  $query = db_select('realname', 'r');
  $names = $query->fields('r', array('uid', 'realname'))
    ->condition('r.realname', '%' . db_like($name) . '%', 'LIKE')
    ->range(0, 10)
    ->execute()
    ->fetchAllKeyed();
  foreach ($names as $uid => $realname) {
    $terms[$realname] = $realname . ' (' . $uid . ')';
  }
  drupal_json_output($terms);
}


/**
 * Implements hook_field_formatter_info().
 */
function realname_autocomplete_widget_field_formatter_info() {
  return array(
    'realname_autocomplete_widget_link' => array(
      'label' => t('Autocomplete Realname Link'),
      'field types' => array('text'),
    ),
    'realname_autocomplete_widget_plain' => array(
      'label' => t('Autocomplete Realname Plain'),
      'field types' => array('text'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function realname_autocomplete_widget_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  switch ($display['type']) {
    case 'realname_autocomplete_widget_link':
      foreach ($items as $delta => $item) {
        $uid = _realname_autocomplete_widget_get_uid($item['safe_value']);
        if (!empty($uid)) {
          $user = user_load($uid);
          $uri = entity_uri('user', $user);
          $element[$delta] = array(
            '#type' => 'link',
            '#title' => $item['safe_value'],
            '#href' => $uri['path'],
            '#options' => $uri['options'],
          );
        }
        else {
          $element[$delta] = array(
            '#markup' => check_plain($item['safe_value']),
          );
        }

      }
      break;
    case 'realname_autocomplete_widget_plain':
      foreach ($items as $delta => $item) {
          $element[$delta] = array(
            '#markup' => check_plain($item['safe_value']),
          );
        }
      break;
  }

  return $element;
}
/**
 * Return uid given realname
 * @param  string $realname realname string
 * @return int           user uid
 */
function _realname_autocomplete_widget_get_uid($realname) {
  $query = db_select('realname', 'r');
  $query->fields('r', array('uid'));
  $query->condition('r.realname', $realname);
  return $query->execute()->fetchField(0);
}
